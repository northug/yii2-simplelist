Meta Tags and Title
===================
Add Meta Tags and Title in model

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist northug/yii2-simplelist "*"
```

or add

```
"northug/yii2-simplelist": "*"
```

to the require section of your `composer.json` file.

Migrate:
```php
yii migrate --migrationPath=@northug/metaTags/migrations
```

Usage
-----

Once the extension is installed, simply use it in your code by  :

Add `MetaTagBehavior` to your model, and configure it.

```php
'controllerMap' => [
    ...
    'simplelist' => [
        'class' => 'northug\simplelist\Controller',
    ]
    ...
],
```

In model

```
public function behaviors() {
    return [
        [
            'class' => ListBehavior::class,
            'attributes' => ['attribute'],
        ],
    ];
}
```

In form

```
<?= $form->field($model, 'attribute')->widget(ListWidget::class, [
        'structure' => [
            ['name' => 'code', 'title' => 'Code city', 'type' => 'text'],
            ['name' => 'phone', 'title' => 'Phone city', 'type' => 'text'],
        ],
    ]) ?>
```
