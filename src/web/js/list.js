$(document).ready(function() {
    
    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };  
    
    $('body').on('click', '.list-table-delete', function(){
        $(this).parent().parent().remove();
        updateNumber($(this).parents('.simplelist'));
    });
    
    $('body').on('click', '.list-table-add', function(){
        
        var structure = $(this).attr('data-structure');
        var formname = $(this).attr('data-formname');
        var attribute = $(this).attr('data-attribute');
        var name = $(this).attr('data-name');
        var table = $(this).attr('table-id');
        var baseUrl = $(this).attr('data-baseUrl');
        var count = parseInt($('#' + table + ' td[number]').length) + 1;

        $.ajax({
            url: '/simplelist/add',
            type: 'POST',
            data : {
                structure: structure,
                formname: formname,
                attribute: attribute,
                name: name,
                baseUrl: baseUrl,
                count: count
            },
            dataType: 'html',
            success: function(result){
                $('#'+table + ' tbody').append(result);
                updateNumber();
                return;
            }
        });
        
    });

    $('.simplelist tbody').sortable({
        helper: fixHelper,
        stop: function() {
            updateNumber();
        }
    });
    
    $('.simplelist').each(function( index ) {
        $(this).find('tbody').sortable({
            helper: fixHelper,
            stop: function() {
                updateNumber($(this));
            }
        });
      });
    
    function updateNumber(object) {
        $(object).find('td[number]').each(function(i, elem) {
            elem.innerHTML = parseInt(i) + 1;
        });
    }
    
});