<?php

namespace northug\simplelist;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Description of ListBehavior
 *
 * @author Пользователь
 */
class ListBehavior extends Behavior {

    /**
     * Атрибуты, которые сериализуются
     * @var array 
     */
    public $attributes = null;

    public function events() {
        if (isset(Yii::$app->request->isPost) and !Yii::$app->request->isPost) {
            return [ActiveRecord::EVENT_AFTER_FIND => 'unserialize'];
        }
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'serialize',
            ActiveRecord::EVENT_AFTER_FIND => 'unserialize',
        ];
    }

    public function serialize() {
        if ($this->attributes === null) {
            return;
        }
        $post = Yii::$app->request->post($this->owner->formName());
        foreach ($this->attributes as $attribute) {
            if (isset($this->owner->{$attribute}) and is_array($this->owner->{$attribute}) and isset($post[$attribute])) {
                $this->owner->{$attribute} = serialize($this->owner->{$attribute});
            } else {
                $this->owner->{$attribute} = null;
            }
        }
    }

    public function unserialize() {
        if ($this->attributes === null) {
            return;
        }
        foreach ($this->attributes as $attribute) {
            if (isset($this->owner->{$attribute}) and ! is_array($this->owner->{$attribute}) and ! is_object($this->owner->{$attribute})) {
                $this->owner->{$attribute} = unserialize($this->owner->{$attribute});
            }
        }
    }

}
