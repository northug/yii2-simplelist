<?php

namespace northug\simplelist;

use yii\base\Exception;
use yii\helpers\ArrayHelper;

class ListWidget extends \yii\widgets\InputWidget {

    /**
     * Структура таблицы
     * 'structure' => [
            ['name' => 'code', 'title' => 'Code city', 'type' => 'text'],
            ['name' => 'phone1', 'title' => 'Phone city1', 'type' => 'number'],
            ['name' => 'phone2', 'title' => 'Phone city2', 'type' => 'select', 'items' => [
                    1 => 'Item 1', 2 => 'Item 2', 5 => 'Item 3', 10 => 'Item 4'
                ]],
            ['name' => 'phone3', 'title' => 'Phone city3', 'type' => 'select', 'items' => [
                    [1 => '123'],
                    [2 => 'sdf'],
                    [3 => '12ndh3'],
                    [4 => '12dt3245343'],
                ]],
            ['name' => 'phone3', 'title' => 'Phone city3', 'type' => 'select', 'items' => [
                    'Block 1' => [1 => '123', 2 => '123'],
                    'Block 2' => [3 => 'sdf'],
                    'Block 3' => [4 => '12ndh3'],
                    'Block 4' => [5 => '12dt3245343'],
     *          ]],
        ],
     * @var array
     */
    public $structure;

    /**
     * Заголовок
     * @var string
     */
    public $title;

    /**
     * Счетчик строки (отображать/не отображать)
     * @var boolean
     */
    public $counterLine = true;

    /**
     * Дефолтное значение для типа колонки
     * @var string
     */
    public $defaultType = 'text';
    
    /**
     * Текущий массив
     * @var array 
     */
    public $data;

    public function init() {
        parent::init();
        $this->guideStructure();
        $this->setDefaultTypeStructure();
        $this->setDefaultTitle();
        $this->data = $this->getData();
    }

    public function run() {
        return $this->render('index', [
                    'model' => $this->model,
                    'name' => $this->name,
                    'title' => $this->title,
                    'attribute' => $this->attribute,
                    'structure' => $this->structure,
                    'data' => $this->data,
                    'counterLine' => $this->counterLine,
        ]);
    }
    
    /**
     * Проверка на наличие структуры данных.
     * @throws Exception
     */
    private function guideStructure() {
        if ($this->structure === null) {
            throw new Exception('Не задана структура таблицы у атрибута {' . $this->attribute . '}', 400);
        }
    }
    
    private function setDefaultTypeStructure() {
        foreach ($this->structure as &$item) {
            if (!isset($item['type'])) {
                $item['type'] = $this->defaultType;
            }
        }
    }

    /**
     * Установка дефолтного заголовка из модели
     */
    private function setDefaultTitle() {
        if ($this->title === null) {
            $this->title = $this->model ? $this->model->getAttributeLabel($this->attribute) : null;
        }
    }
    
    /**
     * Получение текущих данных из модели
     * @return array
     */
    private function getData() {
        if ($this->data !== null) {
            return $this->data;
        }
        if (strpos($this->attribute, ']') !== false) {
            $this->attribute = substr($this->attribute, strpos($this->attribute, ']') + 1, strlen($this->attribute));
        }
        return $this->getValidateData(ArrayHelper::getValue($this->model, $this->attribute));
    }
    
    /**
     * Возвращение корректных данных
     * @param array $data
     * @return array|null
     */
    private function getValidateData($data) {
        if (!$data) {
            return null;
        }
        return !is_array($data) ? array_values(unserialize($data)) : array_values($data);
    }
    
}
