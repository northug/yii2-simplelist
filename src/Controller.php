<?php

namespace northug\simplelist;

use Yii;
use yii\filters\VerbFilter;

/**
 * Description of ListController
 *
 * @author Пользователь
 */
class Controller extends \yii\web\Controller {
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionAdd() {
        $post = Yii::$app->request->post();

        return $this->renderAjax('@northug/simplelist/views/item', [
                    'formName' => $post['formname'],
                    'structure' => unserialize($post['structure']),
                    'baseUrl' => $post['baseUrl'],
                    'attribute' => $post['attribute'],
                    'name' => $post['name'],
                    'countLine' => $post['count'],
                    'counterLine' => true,
                ]);
    }
    
}
