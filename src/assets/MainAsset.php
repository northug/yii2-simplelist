<?php

namespace northug\simplelist\assets;

use yii\web\AssetBundle;

class MainAsset extends AssetBundle
{
    public $sourcePath = '@northug/simplelist/web';
    public $css = [
        'css/list.css',
    ];
    public $js = [
        'js/list.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}