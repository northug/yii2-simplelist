<?php
/* @var $this yii\web\View */
/* @var $model yii\db\ActiveRecord */
/* @var $structure array */
/* @var $data array */
/* @var $name string */
/* @var $title string */
/* @var $attribute string */
/* @var $counterLine boolean */

use yii\helpers\Html;
use northug\simplelist\assets\MainAsset;

$bundle = MainAsset::register($this);

$table_id = $model ? 'list-' . strtolower($model->formName()) . '-' . $attribute : md5('list-' . strtolower($name));
$class = $model ? strtolower($model->formName()) . '-' . $attribute : md5('list-' . strtolower($name));

if (!$model) {
    echo Html::tag('label', $title);
}

?>
<div class="form-group field-<?php echo $class ?>">

    <table id="<?= $table_id ?>" class="table table-bordered table-hover table-xs simplelist">
        <thead>
            <tr>
                <?php
                if ($counterLine) {
                    echo Html::tag('th', '#', ['width' => 1]);
                }
                foreach ($structure as $key => $itemHeader) {
                    echo Html::tag('th', $itemHeader['title']);
                }
                echo Html::tag('th', '', ['class' => 'list-delete', 'width' => 50]);
                echo Html::tag('th', '', ['class' => 'list-sortable', 'width' => 5]);
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $countLine = 1;
            if ($data) {
                foreach ($data as $line) {
                    echo $this->render('item', [
                        'formName' => $model ? $model->formName() : null,
                        'structure' => $structure,
                        'line' => $line,
                        'name' => $name,
                        'countLine' => $countLine,
                        'attribute' => $attribute,
                        'counterLine' => $counterLine,
                        'baseUrl' => $bundle->baseUrl,
                    ]);
                    $countLine++;
                }
            }
            ?>
        </tbody>
    </table>

    <?=
    Html::Button('Добавить строку', [
        'class' => 'btn btn-success list-table-add',
        'data-structure' => serialize($structure),
        'data-formname' => $model ? $model->formName() : null,
        'data-attribute' => $attribute,
        'data-name' => $name,
        'table-id' => $table_id,
        'data-baseUrl' => $bundle->baseUrl,
    ])
    ?>
    <div class="help-block"></div>
</div>
<hr>