<?php

/* @var $formName string */
/* @var $attribute string */
/* @var $structure array */
/* @var $line array */
/* @var $countLine integer */
/* @var $counterLine boolean */
/* @var $baseUrl string */
/* @var $name string */

use yii\helpers\Html;

if ($structure) {
    echo Html::beginTag('tr');
    if ($counterLine) {
        echo Html::tag('td', $countLine, ['number' => true]);
    }
    foreach ($structure as $key => $itemHeader) {
        $resultName = !$formName ? $name . '[' . $countLine . '][' . $itemHeader['name'] . ']' : $formName . '[' . $attribute . '][' . $countLine . '][' . $itemHeader['name'] . ']';
        echo $this->render('types/' . $itemHeader['type'], [
            'name' => $resultName,
            'value' => isset($line[$itemHeader['name']]) ? $line[$itemHeader['name']] : null,
            'structure' => $itemHeader,
        ]);
    }
    echo Html::tag('th', Html::tag('i', '', ['class' => 'glyphicon glyphicon-remove-circle list-table-delete']), ['class' => 'item-delete']);
    echo Html::tag('th', Html::img($baseUrl . '/img/points.png'), ['class' => 'sortable-move']);
    $countLine++;
    echo Html::endTag('tr');
}
