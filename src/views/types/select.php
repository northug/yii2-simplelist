<?php

/* @var $name string */
/* @var $value string */
/* @var $structure array */

use yii\helpers\Html;

if (isset($structure['items']) and !empty($structure['items']) and is_array($structure['items'])) {
    echo Html::tag('th', Html::dropDownList($name, $value, $structure['items'], ['class' => 'form-control']));
} else {
    echo Html::tag('th', '');
}
