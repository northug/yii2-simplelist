<?php

/* @var $name string */
/* @var $value string */
/* @var $structure array */

use yii\helpers\Html;

echo Html::beginTag('th');
if ($value) {
    echo Html::checkbox($name, true, ['class' => 'form-control']);
} else {
    echo Html::checkbox($name, false, ['class' => 'form-control']);
}
echo Html::endTag('th');
