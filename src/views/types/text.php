<?php

/* @var $name string */
/* @var $value string */
/* @var $structure array */

use yii\helpers\Html;

echo Html::tag('th', Html::input('text', $name, $value, ['class' => 'form-control']));
